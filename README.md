# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Create scripts to allow people to write C and Fortran in languages other than English
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download the repository or edit online. Make a pull request for your contributions to be added or ask to be added to the project.

### Contribution guidelines ###

* Try to follow current structure - translation of list of keywords, translation script, hello world example
* All language contributions welcome

### TODO ###

* what if a variable name conflicts with a original keyword
* handling of 2-word keywords
* Readme in the translated languages

### Who do I talk to? ###

* Repo owner or admin: Benson Muite. benson.muite@ut.ee

### Acknowledgements ###

* Dominik Goeddeke