#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program converts a Fortran program written using specified Arabic keywords to a regular
# Fortran program using English keywords
# Since Arabic has characters that are not in the regular ascii format, utf-8 formatting
# is used. More information on specifying Python formatting options is at
# http://legacy.python.org/dev/peps/pep-0263/
# 
# An example Arabic Fortran program is in helloworldArabic.f90
#
# To convert the program type
# >python convertArabicEnglish.py  helloworldArabic.f90
#
# the output should be a file called helloworldArabicconvertedEnglish.f90
#
# To compile using a regular Fortran compiler, eg gfortran,
# >gfortran helloworldArabicconvertedEnglish.f90
#
# running the resulting binary should produce the output "مرحبا العالم"
#

import sys, math, string, codecs
usage = 'Usage: %s infile' % sys.argv[0]

try:
    infilename = sys.argv[1]
except:
    print usage; sys.exit(1)


# read English keywords
readfilename = "fortrankeywordsEnglish.txt"
infileEnglish=open(readfilename,'r')    # open file for reading
linecount=0                             # counter for English keywords
EnglishList=[]                          # create empty list to store English keywords
for line in infileEnglish:
    EnglishList.append(line.strip())    # read in lines and ignore blank spaces
    linecount=linecount+1

infileEnglish.close()

# read Arabic keywords
readfilename = "fortrankeywordsArabic.txt"
infileArabic=codecs.open(readfilename,'r',encoding='utf-8')     # open file for reading
linecount2=0                            # counter for Arabic keywords
ArabicList=[]                           # create empty list to store Arabic keywords
for line in infileArabic:
    ArabicList.append(line.strip())     # read in lines and ignore blank spaces
    linecount2=linecount2+1

infileArabic.close()

if linecount != linecount2:
   print "Error, the list of keywords are not the same length"
   sys.exit(1)

ifile = codecs.open(infilename, 'r',encoding='utf-8')   # open file for reading
lines=ifile.readlines()
ifile.close()

outfilename=infilename.replace('.f90','convertedEnglishUTF8.f90')
outfile = codecs.open(outfilename,'w',encoding='utf-8')

for line in lines:
     linecount3=0   		# reset counter, then check and replace keywords
     while linecount3 < linecount2:
           line = line.replace(ArabicList[linecount3],EnglishList[linecount3])
           linecount3=linecount3+1
     outfile.write(line)        # writeout updated keywords
     outfile.write('\n')

outfile.close()
outfile = codecs.open(outfilename,'r',encoding='utf-8').read()

#outfile2=outfile.encode('ascii','backslashreplace')

outfilename2 = infilename.replace('.f90','convertedAllEnglish.f90')
#codecs.open(outfilename2,'w',encoding='utf-8').write(outfile2)

ifile = codecs.open(outfilename, 'r',encoding='utf-8')   # open file for reading
lines=ifile.readlines()
ifile.close()
outfile = codecs.open(outfilename2,'w',encoding='utf-8')


for line in lines:
     checkprint = line.find(' " ')
     if checkprint == -1 :
             line=line.encode('ascii','backslashreplace')
             line = line.replace('\u','Slashu')
             cols = line.split()                # split line at spaces
             cols.reverse() 	                # reverse order or arguments
             temp = ' '.join(cols)
             temp=temp.encode('utf-8')
             outfile.write(temp)                # writeout updated keywords
             outfile.write('\n')
     else :
              prints = line.split('"')          # split line at text delimiters 
              cols = prints[2].encode('ascii')
              cols = cols.replace('\u','Slashu')
              cols = cols.split() 				# split at spaces
              cols.reverse() 	        		# reverse order or arguments     
              temp = ' '.join(cols)
              temp = temp.encode('utf-8')
              temp = [temp,' " ']
              temp = ' '.join(temp)
              temp = [temp,prints[1]]
              temp = ' '.join(temp)
              temp = [temp,' " ']
              temp = ' '.join(temp)
              outfile.write(temp)        # writeout updated keywords
              outfile.write('\n')

outfile.close()

#ifile = codecs.open(outfilename2, 'r',encoding='ascii').read()   # open file for reading

#outfile=ifile.encode('utf-8')
#codecs.open(outfilename2,'w',encoding='utf-8').write(outfile)