#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program converts a Fortran program written using specified German keywords to a regular
# Fortra program using English keywords
# Since German has characters that are not in the regular ascii format, utf-8 formatting
# is used. More information on specifying Python formatting options is at
# http://legacy.python.org/dev/peps/pep-0263/
# 
# An example German Fortran program is in helloworldGerman.f90
#
# To convert the program type
# >python convertGermanEnglish.py  helloworldGerman.f90
#
# the output should be a file called helloworldGermanconvertedEnglish.f90
#
# To compile using a regular Fortran compiler, eg gfortran,
# >gfortran helloworldGermanconvertedEnglish.f90
#
# running the resulting binary should produce the output "Tere Maalim"
#

import sys, math, string
usage = 'Usage: %s infile' % sys.argv[0]

try:
    infilename = sys.argv[1]
except:
    print usage; sys.exit(1)


# read English keywords
readfilename = "FortranKeywordsEnglish.txt"
infileEnglish=open(readfilename,'r')    # open file for reading
linecount=0                             # counter for English keywords
EnglishList=[]                          # create empty list to store English keywords
for line in infileEnglish:
    EnglishList.append(line.strip())    # read in lines and ignore blank spaces
    linecount=linecount+1

infileEnglish.close()

# read German keywords
readfilename = "FortranKeywordsGerman.txt"
infileGerman=open(readfilename,'r')     # open file for reading
linecount2=0                              # counter for German keywords
GermanList=[]                           # create empty list to store German keywords
for line in infileGerman:
    GermanList.append(line.strip())     # read in lines and ignore blank spaces
    linecount2=linecount2+1

infileGerman.close()

if linecount != linecount2:
   print "Error, the list of keywords are not the same length"+str(linecount)+","+str(linecount2)
   sys.exit(1)

ifile = open(infilename, 'r')   # open file for reading
lines=ifile.readlines()
ifile.close()

outfilename=infilename.replace('.f90','convertedEnglish.f90')
outfile = open(outfilename,'w')

for line in lines:
     linecount3=0   		# reset counter, then check and replace keywords
     while linecount3 < linecount2:
           line = line.replace(GermanList[linecount3],EnglishList[linecount3])
           linecount3=linecount3+1
     outfile.write(line)        # writeout updated keywords
     outfile.write('\n')

outfile.close()
