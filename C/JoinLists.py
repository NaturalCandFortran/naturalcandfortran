#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Python 2.7 program joins 2 lists of keywords, one for English and the second obtained
# by automatic translation of the English using either Google Translate or Bing Translator. 
# 
# To create a joined combined key word list type
# >python JoinLists.py ckeywordsSerbian2 
#
# the output should be a file called ckeywordsSerbian which combines both English and Serbian
# Keywords 
#

import sys, math, string
usage = 'Usage: %s infile' % sys.argv[0]

try:
    infilename = sys.argv[1]
except:
    print usage; sys.exit(1)


# read English keywords
readfilename = "ckeywords"
infileEnglish=open(readfilename,'r')    # open file for reading
linecount=0                             # counter for English keywords
EnglishList=[]                          # create empty list to store English keywords
for line in infileEnglish:
    EnglishList.append(line.strip())    # read in lines and ignore blank spaces
    linecount=linecount+1

infileEnglish.close()

# read Other keywords
readfilename = infilename
infileOther=open(readfilename,'r')     # open file for reading
linecount2=0                           # counter for other keywords
OtherList=[]                           # create empty list to store other keywords
for line in infileOther:
    OtherList.append(line.strip())     # read in lines and ignore blank spaces
    linecount2=linecount2+1

infileOther.close()

if linecount != linecount2:
   print "Error, the list of keywords are not the same length"
   sys.exit(1)

ifile = open(infilename, 'r')   # open file for reading
lines=ifile.readlines()
ifile.close()

outfilename=infilename.replace('2','')
outfile = open(outfilename,'w')

linecount3=0
for line in lines:
     keywords = str(EnglishList[linecount3])+str("    ")+str(OtherList[linecount3])
     outfile.write(keywords)        # writeout updated keywords
     outfile.write('\n')
     linecount3=linecount3+1

outfile.close()
