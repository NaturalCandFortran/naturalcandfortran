*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * for any question about the script bellow 
 * feel free to comunicate via h.almehmadi@yahoo.com
 * enjoy ;)
 * 
 */
package carengtransilationjava;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author hana abdullah
 */
public class CArEngTransilationJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        // TODO code application logic here
 

        ArrayList arList = new ArrayList();
        ArrayList enList = new ArrayList();
      
        
        
        //raed the arabic c keyword file
        BufferedReader abr = null;
 
		try {
                   
 
			String sCurrentLine;
 
			abr = new BufferedReader(new FileReader("ckeywordsAr.txt"));
 
			while ((sCurrentLine = abr.readLine()) != null) {
			
                        arList.add(sCurrentLine);	
                    
                       
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
                    try {
				if (abr != null)abr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
                
                    
                
                //raed the  english c keyword file
                
                 BufferedReader ebr = null;
 
		try {
 
			String sCurrentLine;
 
			ebr = new BufferedReader(new FileReader("ckeywordsEn.txt"));
 
			while ((sCurrentLine = ebr.readLine()) != null) {
				
			 enList.add(sCurrentLine);	
                        }
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ebr != null)ebr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
                
              // check if the size of the english and arabic keywords file match 
                if(enList.size()!=arList.size())
                {
                System.out.println("Sorry mismatch the length of arabic/english keywords");
                System.exit(1);
                }
                          
                //creat the output file as .c utf-8
                PrintWriter writer = new PrintWriter("OutputCapp.c", "UTF-8");


//reading the arabic c application file
                
                 BufferedReader appbr = null;
 
		try {
 
			String sCurrentLine="";

			appbr = new BufferedReader(new FileReader("theapp.txt"));
 
			while ((sCurrentLine = appbr.readLine()) != null) {
			
                            for( int count=0; count<arList.size();count++){
                            sCurrentLine=sCurrentLine.replaceAll(arList.get(count).toString(),enList.get(count).toString());
                            }
                            writer.println(sCurrentLine);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (appbr != null)appbr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
      writer.close();
    
    
    
    }
}