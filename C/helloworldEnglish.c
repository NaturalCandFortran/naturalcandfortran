/* from http://www.roesler-ac.de/wolfram/hello.htm
   checked on 23 July 2014

   see also 
   https://en.wikipedia.org/wiki/Hello_world_program 
   checked on 23 July 2014
*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
 {
  puts("Hellow World!");
  return EXIT_SUCCESS;
 }

