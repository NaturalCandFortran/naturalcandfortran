#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program converts a C program written using specified Mandarin keywords to a regular
# C program using English keywords
# Since Mandarin has characters that are not in the regular ascii format, utf-8 formatting
# is used. More information on specifying Python formatting options is at
# http://legacy.python.org/dev/peps/pep-0263/
# 
# An example Mandarin C program is in helloworldMandarin.c
#
# To convert the program type
# >python convertMandarinEnglish.py  helloworldMandarin.c
#
# the output should be a file called helloworldMandarinconvertedEnglish.c
#
# To compile using a regular c compiler, eg gcc,
# >gcc helloworldMandarinconvertedEnglish.c
#
# running the resulting binary should produce the output "你好世界!"
#

import sys, math, string
usage = 'Usage: %s infile' % sys.argv[0]

try:
    infilename = sys.argv[1]
except:
    print usage; sys.exit(1)


# read English keywords
readfilename = "ckeywordsEnglish.txt"
infileEnglish=open(readfilename,'r')    # open file for reading
linecount=0                             # counter for English keywords
EnglishList=[]                          # create empty list to store English keywords
for line in infileEnglish:
    EnglishList.append(line.strip())    # read in lines and ignore blank spaces
    linecount=linecount+1

infileEnglish.close()

# read Mandarin keywords
readfilename = "ckeywordsMandarin.txt"
infileMandarin=open(readfilename,'r')     # open file for reading
linecount2=0                              # counter for Mandarin keywords
MandarinList=[]                           # create empty list to store Mandarin keywords
for line in infileMandarin:
    MandarinList.append(line.strip())     # read in lines and ignore blank spaces
    linecount2=linecount2+1

infileMandarin.close()

if linecount != linecount2:
   print "Error, the list of keywords are not the same length"
   sys.exit(1)

ifile = open(infilename, 'r')   # open file for reading
lines=ifile.readlines()
ifile.close()

outfilename=infilename.replace('.c','convertedEnglish.c')
outfile = open(outfilename,'w')

for line in lines:
     linecount3=0   		# reset counter, then check and replace keywords
     while linecount3 < linecount2:
           line = line.replace(MandarinList[linecount3],EnglishList[linecount3])
           linecount3=linecount3+1
     outfile.write(line)        # writeout updated keywords
     outfile.write('\n')

outfile.close()
