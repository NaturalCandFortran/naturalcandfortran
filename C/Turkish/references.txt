General References

https://en.wikibooks.org/wiki/C_Programming/Reference_Tables
http://www.dict.cc/?s=

H.P. Langtangen "Python scripting for computational science" 3rd Ed. Springer 2007
H.P. Langtangen "A primer on scientific programming with Python" 2nd Ed. Springer 2011
Programs and errata at 
http://vefur.simula.no/intro-programming/
D.M. Beazley "Python Essential Reference" 4th Ed. Addison-Wesley 2009
M. Lutz "Learning Python" 4th Ed. O'Reilly 2009

http://legacy.python.org/dev/peps/pep-0263/
http://www.microsoft.com/Language/en-US/Terminology.aspx
http://hindawi.in
http://www.bing.com/translator/
Google Translate https://translate.google.com
http://translate.yandex.com/

Albanian
http://translate.yandex.com/

Arabic
Computer Dictionary English-Arabic, Arab Scientific Publisher, ISBN 2-84409-163-6
Fortran77 with Scientific & Engineering Applications (Arabic Version), Copyright Awad Mansour, ISBN 90-6789-048-0

Belarusian
http://translate.yandex.com/

Catalan
http://translate.yandex.com/

Chinese

Czech
http://translate.yandex.com/

Estonian
http://dict.ibs.ee/index.html
R, Kask, R. Soosar, ``TEA taskusõnastik Inglise-eesti / eesti-inglise''
K. Kiik, R. Mägi,K. Raudvere, I. Hein, M. Kernumees, E. Priedenthal, T. Vassiljeva Eds., TEA Kirjastus (2014) 
ISBN 978-9985-71-307-5

French
R. Alderman, F. Auvrai, J. Berg, R. Gachod-Schinko, K. Hald, M. Kopyczinski, A. Leyrat, B. Simon, R. Urborn, C.W. Reul,
"Barron's French-English Dictionary" M. Dischler Ed.,  Barron's Educational Series (2006) ISBN 978-0-7641-3330-5


German


Hebrew
http://www.bing.com/translator/

Hindi
https://translate.google.com/

Hungarian
http://translate.yandex.com/

Igbo
https://translate.google.com/

Italian
https://translate.google.com/

Kinyarwanda
http://kinyarwanda.net/

Kiswahili
http://glosbe.com/en/sw/

Latvian
R. Mozere, A Millere, "Angļu-Latviešu, Latviešu-Angļu Vārdnīca" Apgāds Zvaigzne ABC (2002) ISBN 9984-17-962-1
I. Matsione, I. Bliumfelde, L. Krieviņa, A. ēbere, M. Ozola 
"Angļu-Latviešu starputiskās uzņēmējdarbības terminu vārdnīca" Apgāds Zvaigzne ABC (2002) ISBN 9984-36-736-3

Lithuanian
http://www.bing.com/translator/

Luo
http://www.freewebs.com/themulders/luoguide.htm

Norwegian
https://translate.google.com/

Polish
http://translate.yandex.com/

Portugese
http://www.bing.com/translator/

Russian
http://www.bing.com/translator/

Serbian
https://translate.google.com/

Somali
https://translate.google.com/

Tagalog
https://translate.google.com/

Tamil
https://translate.google.com/

Turkish
http://translate.yandex.com/

Ukrainian
http://www.bing.com/translator/ 

Vietnamese
http://translate.yandex.com/

Zulu
https://translate.google.com/

